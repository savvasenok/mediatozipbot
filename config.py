from os.path import abspath, dirname, join
from random import choice
from string import ascii_letters, digits


class Config:
    SECRET_LENGTH = 16

    class Folder:
        root = abspath(dirname(__file__))
        media = join(root, "media")
        zips = join(root, "zips")

        def list_folder_content(folder: str) -> list:
            pass

        def file_exists(folder: str, file: str) -> bool:
            pass


class BotConfig:
    BOT_TOKEN = "1391291060:AAGq-VWcs-HmHlplaNhCrouR449hv8ghQZw"
    DEFAULT_PARSE_MODE = "HTML"
    THREADED = False
    SKIP_PENDING = False

    URL = "https://mediatozipbot.savvamirzoyan.xyz/"
    SECRET = "".join(
        choice(ascii_letters + digits)
        for i in range(Config.SECRET_LENGTH)
    )


class UserConfig:
    pass


class AppConfig:
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///"+join(Config.Folder.root, "main.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "".join(
        choice(ascii_letters + digits)
        for i in range(Config.SECRET_LENGTH)
    )

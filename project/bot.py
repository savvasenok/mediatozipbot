from telebot import TeleBot
from config import BotConfig


bot = TeleBot(
    token=BotConfig.BOT_TOKEN,
    parse_mode=BotConfig.DEFAULT_PARSE_MODE,
    threaded=BotConfig.THREADED,
    skip_pending=BotConfig.SKIP_PENDING
)

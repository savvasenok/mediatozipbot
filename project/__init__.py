from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from telebot import types
from project.bot import bot
from config import AppConfig, BotConfig


app = Flask(__name__)
app.config.from_object(AppConfig)

db = SQLAlchemy(app)


from handlers.command_handler import *


@app.route("/")
def index():
    return "ok", 200


@app.route("/" + BotConfig.SECRET, methods=["POST"])
def webhook():
    update = types.Update.de_json(request.stream.read().decode("UTF-8"))
    bot.process_new_updates((update))

    return "ok", 200
